var app = angular.module('noteApp', ['ngRoute', 'ngResource', 'controllers']);

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/user', {
		templateUrl: '/partials/user.html',
		controller: 'UserController'
	}).
	when('/note', {
		templateUrl: '/partials/note.html',
		controller: 'NoteController'
	}).
	otherwise({
		redirectTo: '/user'
	});
}]);