var controllers = angular.module('controllers', ['ngRoute', 'resources']);

controllers.controller('UserController', ['Users', '$scope', '$location', function (Users, $scope, $location) {

	var clear = function() {
		
		$scope.user = undefined;
		$scope.editMode = false;
		$scope.txtLogin = '';
		$scope.txtName = '';
		$scope.txtEmail = '';
		$scope.txtPassword = '';
  	
	};
	this.cancel = clear;

	var loadUsers = function() {
		$scope.users = Users.query();
	};
	this.loadUsers = loadUsers;
  
	this.save = function() {
		var user = {login: $scope.txtLogin, 
		            email: $scope.txtEmail, 
		            password: $scope.txtPassword, 
		            name: $scope.txtName};
		
		Users.save(user, function() {
			clear();
			loadUsers();
		});
	};
  
	this.update = function() {
		var user = {login: $scope.txtLogin, 
                	email: $scope.txtEmail, 
                	password: $scope.txtPassword, 
                	name: $scope.txtName};
    
		Users.update({id: $scope.user.id}, user, function(){
			clear();
			loadUsers();
		});
	};
  
	this.delete = function(id) {
		Users.delete({id: id}, loadUsers);
	};
  
	this.set = function(id) {
		Users.set({id: id}, function(u){
			$scope.user = u;
			$location.path('/note').replace();
		});
	};
  
	this.load = function(user) {
		$scope.user = user;
		$scope.editMode = true;
		$scope.txtLogin = user.login;
		$scope.txtName = user.name;
		$scope.txtEmail = user.email;
		$scope.txtPassword = '';
	};
  
	clear();
	loadUsers();
 
}]);

controllers.controller('NoteController', ['Notes', '$scope', function (Notes, $scope) {

	var clear = function() {
		$scope.editMode = false;
		$scope.txtDescription = '';
		$scope.txtNote = '';
	};  
	this.cancel = clear;

	var loadNotes = function() {
		$scope.notes = Notes.query();
	}; 
	this.loadNotes = loadNotes;
  
	this.save = function() {
		var note = {description: $scope.txtDescription, note: $scope.txtNote};
		Notes.save(note, function() {
			clear();
			loadNotes();
		});
	};
  
	this.update = function(id) {
		var note = {description: $scope.txtDescription, note: $scope.txtNote};
		Notes.update({id: id}, note, function() {
			clear();
			loadNotes();
		});
	};
  
	this.delete = function(id) {
		Notes.delete({id: id}, loadNotes);
	};
  
	this.load = function(note) {
		$scope.editMode = true;
		$scope.txtDescription = note.description;
		$scope.txtNote = note.note;
	};
  
	clear();
	loadNotes();
 
}]);