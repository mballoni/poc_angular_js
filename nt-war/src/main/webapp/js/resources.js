var resources = angular.module('resources', []);

resources.factory('Users', ['$resource', function($resource) {
	return $resource('/rest/user/:id', null, {
		'update': {method: 'PUT'},
		'set': {method: 'GET', url: '/rest/user/set/:id', isArray: false} 
	});
}]);

resources.factory('Notes', ['$resource', function($resource) {
	return $resource('/rest/note/:id', null, {
		'update': {method: 'PUT'}
	});
}]);

