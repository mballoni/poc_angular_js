package com.cjm.nt.entity;

import java.util.ArrayList;
import java.util.List;

import com.vercer.engine.persist.annotation.Embed;
import com.vercer.engine.persist.annotation.Parent;

public class Note extends AbstractEntity {

	@Parent
	private User owner;

	private String description;
	private String note;

	@Embed
	private List<Tag> tags = new ArrayList<Tag>();

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<Tag> getTags() {
		return tags;
	}

}
