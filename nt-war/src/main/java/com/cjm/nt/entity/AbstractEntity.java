package com.cjm.nt.entity;

import java.util.Date;

import com.vercer.engine.persist.annotation.Key;

public abstract class AbstractEntity implements Entity {

	@Key
	private String id;
	private boolean enabled = true;
	private Date createDate = new Date();
	private Date updateDate = new Date();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
