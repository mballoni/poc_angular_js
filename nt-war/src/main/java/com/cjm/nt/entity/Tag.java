package com.cjm.nt.entity;

public class Tag extends AbstractEntity {

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
