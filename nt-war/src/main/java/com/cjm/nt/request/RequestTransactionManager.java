package com.cjm.nt.request;

import java.util.HashMap;
import java.util.Map;

public class RequestTransactionManager {

	private static final RequestTransactionManager instance = new RequestTransactionManager();

	private final ThreadLocal<Map<ContentKeyEnum, Object>> contents;

	/**
	 * Create the ThreadLocal instance
	 */
	private RequestTransactionManager() {
		contents = new ThreadLocal<Map<ContentKeyEnum, Object>>();
	}

	/**
	 * Set the transaction id to the ThreadLocal
	 * 
	 * @param tid
	 *            - Transaction Id
	 * @return Transaction Id
	 */
	public void put(ContentKeyEnum key, final Object value) {

		if (contents.get() == null) {
			contents.set(new HashMap<RequestTransactionManager.ContentKeyEnum, Object>());
		}

		contents.get().put(key, value);
	}

	public Object get(ContentKeyEnum key) {
		return contents.get().get(key);
	}

	/**
	 * Clean the ThreadLocal
	 */
	public void clean() {
		contents.remove();
	}

	/**
	 * Return the singleton instance
	 * 
	 * @return ThreadLocal instance
	 */
	public static RequestTransactionManager instance() {
		return instance;
	}

	public enum ContentKeyEnum {
		DATA_STORE;
	}

}
