package com.cjm.nt.request.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.cjm.nt.dao.ObjectDatastoreProvider;
import com.cjm.nt.request.RequestTransactionManager;
import com.cjm.nt.request.RequestTransactionManager.ContentKeyEnum;

public class RequestFilter implements Filter {

	private RequestTransactionManager transactionManager = null;

	public RequestFilter() {
		transactionManager = RequestTransactionManager.instance();
	}

	public void destroy() {

	}

	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		try {

			transactionManager.put(ContentKeyEnum.DATA_STORE,
					ObjectDatastoreProvider.getObjectDatastore());

			chain.doFilter(req, resp);

		} finally {

			transactionManager.clean();

		}

	}

	public void init(FilterConfig arg0) throws ServletException {

	}

}
