package com.cjm.nt.dao;

import java.util.List;

import com.cjm.nt.entity.Entity;

public interface Dao<E extends Entity> {

	E save(E instance);

	void save(List<E> instances);

	List<E> findAll();

	E update(E entity);

	E findById(String id);

	void delete(E entity);

	void deleteById(String id);

}
