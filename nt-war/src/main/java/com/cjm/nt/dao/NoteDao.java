package com.cjm.nt.dao;

import java.util.List;

import com.cjm.nt.entity.Note;
import com.cjm.nt.entity.User;

public interface NoteDao extends Dao<Note> {

	List<Note> findAll(User user);

	Note findById(User user, String id);

}
