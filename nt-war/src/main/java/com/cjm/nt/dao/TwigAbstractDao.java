package com.cjm.nt.dao;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import com.cjm.nt.entity.AbstractEntity;
import com.cjm.nt.request.RequestTransactionManager;
import com.cjm.nt.request.RequestTransactionManager.ContentKeyEnum;
import com.cjm.nt.util.IdentifierGenerator;
import com.google.common.collect.Lists;
import com.vercer.engine.persist.ObjectDatastore;

public abstract class TwigAbstractDao<E extends AbstractEntity> implements
		Dao<E> {

	protected final Class<E> clazz;

	public TwigAbstractDao(Class<E> typeParameterClass) {
		this.clazz = typeParameterClass;
	}

	protected ObjectDatastore dataStore() {
		return (ObjectDatastore) RequestTransactionManager.instance().get(
				ContentKeyEnum.DATA_STORE);
	}

	public E save(E instance) {

		if (StringUtils.isBlank(instance.getId())) {
			instance.setId(IdentifierGenerator.getId());
		}

		dataStore().store().instance(instance).ensureUniqueKey().returnKeyNow()
				.getAppId();
		return instance;
	}

	public void save(List<E> instances) {

		for (E instance : instances) {
			if (StringUtils.isBlank(instance.getId())) {
				instance.setId(IdentifierGenerator.getId());
			}
		}

		dataStore().storeAll(instances);
	}

	private Iterator<E> findAllByIterator() {
		return dataStore().find(this.clazz);
	}

	public List<E> findAll() {
		return Lists.newArrayList(findAllByIterator());
	}

	public E update(E updateEntity) {

		E entity = findById(updateEntity.getId());
		BeanUtils.copyProperties(updateEntity, entity);
		entity.setUpdateDate(new Date());

		dataStore().update(entity);
		return entity;
	}

	public E findById(String id) {
		return dataStore().load(this.clazz, id);
	}

	public void delete(E entity) {
		dataStore().delete(findById(entity.getId()));
	}

	public void deleteById(String id) {
		dataStore().delete(findById(id));
	}
}
