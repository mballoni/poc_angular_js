package com.cjm.nt.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cjm.nt.dao.NoteDao;
import com.cjm.nt.dao.TwigAbstractDao;
import com.cjm.nt.entity.Note;
import com.cjm.nt.entity.User;
import com.google.common.collect.Lists;

@Component
public class NoteDaoImpl extends TwigAbstractDao<Note> implements NoteDao {

	public NoteDaoImpl() {
		super(Note.class);
	}

	public List<Note> findAll(User user) {

		return Lists.newArrayList(dataStore().find().type(Note.class)
				.withAncestor(user).returnResultsNow());
	}

	public Note findById(User user, String id) {

		Note note = this.findById(id);
		if (note.getOwner().equals(user)) {
			return note;
		}

		return null;
	}
}
