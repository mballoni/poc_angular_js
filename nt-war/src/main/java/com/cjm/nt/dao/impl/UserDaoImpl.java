package com.cjm.nt.dao.impl;

import org.springframework.stereotype.Component;

import com.cjm.nt.dao.TwigAbstractDao;
import com.cjm.nt.dao.UserDao;
import com.cjm.nt.entity.User;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.vercer.engine.persist.FindCommand.RootFindCommand;

@Component
public class UserDaoImpl extends TwigAbstractDao<User> implements UserDao {

	public UserDaoImpl() {
		super(User.class);
	}

	public User findUserByLogin(String login) {
		return buildFilterByLogin(login).maximumResults(1).returnResultsNow()
				.next();
	}

	private RootFindCommand<User> buildFilterByLogin(String login) {
		return dataStore().find().type(this.clazz)
				.addFilter("login", FilterOperator.EQUAL, login);
	}

	private boolean foundUserByLogin(String login) {
		return buildFilterByLogin(login).countResultsNow() > 0;
	}

	@Override
	public User save(User instance) {

		if (!foundUserByLogin(instance.getLogin())) {
			return super.save(instance);
		}

		return null;
	}
}
