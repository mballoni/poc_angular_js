package com.cjm.nt.dao;

import com.vercer.engine.persist.ObjectDatastore;
import com.vercer.engine.persist.annotation.AnnotationObjectDatastore;

public class ObjectDatastoreProvider {

	public static ObjectDatastore getObjectDatastore() {
		return new AnnotationObjectDatastore();
	}

}
