package com.cjm.nt.dao;

import com.cjm.nt.entity.User;

public interface UserDao extends Dao<User> {

	User findUserByLogin(String login);

}
