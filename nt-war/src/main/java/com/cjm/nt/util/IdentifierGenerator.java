package com.cjm.nt.util;

import java.math.BigInteger;
import java.util.UUID;

public final class IdentifierGenerator {

	private static final int BASE_16 = 16;

	private IdentifierGenerator() {
		super();
	}

	/**
	 * Generate unique identify for cart and cart item. (local machine) as a
	 * base for creating the cart ID. This number is coded as a 25-digits number
	 * on base.
	 */
	public static String getId() {

		// stat with a 128-bits random number
		UUID uuid = UUID.randomUUID();
		long lo = uuid.getLeastSignificantBits();
		long hi = uuid.getMostSignificantBits();

		// convert it on an intermediate BigInteger
		String base16 = Long.toHexString(hi) + Long.toHexString(lo);
		BigInteger bigi = new BigInteger(base16, BASE_16);

		// convert it on a 36-base number
		return bigi.toString(Character.MAX_RADIX);
	}
}
