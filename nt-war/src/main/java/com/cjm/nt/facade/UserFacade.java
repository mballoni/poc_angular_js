package com.cjm.nt.facade;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjm.nt.entity.User;
import com.cjm.nt.facade.json.UserJson;
import com.cjm.nt.service.CrudService;
import com.cjm.nt.service.UserService;

@Component
@Path("/user")
public class UserFacade extends CrudAbstractFacade<UserJson, User> {

	public UserFacade() {
		super(UserJson.class, User.class);
	}

	@Autowired
	private UserService userService;

	@Override
	protected CrudService<User> service() {
		return this.userService;
	}

	@Override
	protected User customConverter(UserJson json) {
		return null;
	}

	@Override
	protected UserJson customConverter(User entity) {
		return null;
	}

	@GET
	@Path("/set/{id}")
	public Response setUser(@PathParam("id") String id) {
		return Response.status(Status.OK).entity(userService.findById(id))
				.cookie(generateUserCookie(id)).build();
	}

}
