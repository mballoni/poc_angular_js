package com.cjm.nt.facade;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;

import org.springframework.beans.factory.annotation.Autowired;

import com.cjm.nt.service.UserService;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public abstract class AbstractFacade {

	public static final String COOKIE_GUID = "guid";

	@Autowired
	private UserService userService;

	public NewCookie generateUserCookie(String userId) {
		return new NewCookie(new Cookie(COOKIE_GUID, userService.findById(
				userId).getId(), "/rest", null));
	}
}
