package com.cjm.nt.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.BeanUtils;

import com.cjm.nt.entity.Entity;
import com.cjm.nt.facade.json.Json;
import com.cjm.nt.service.CrudService;

public abstract class CrudAbstractFacade<J extends Json, E extends Entity>
		extends AbstractFacade {

	private Class<E> entityClazz;
	private Class<J> jsonClazz;

	public CrudAbstractFacade(Class<J> jsonClazz, Class<E> entityclazz) {
		this.entityClazz = entityclazz;
		this.jsonClazz = jsonClazz;
	}

	/**
	 * @return
	 */
	protected abstract CrudService<E> service();

	/**
	 * @param json
	 * @return
	 */
	protected abstract E customConverter(J json);

	protected E converter(J json) throws InstantiationException,
			IllegalAccessException {

		E customConvertedBean = customConverter(json);
		if (customConvertedBean == null) {
			E convertedBean = this.entityClazz.newInstance();
			BeanUtils.copyProperties(json, convertedBean);
			return convertedBean;

		}

		return customConvertedBean;

	}

	protected abstract J customConverter(E entity);

	protected J converter(E entity) throws InstantiationException,
			IllegalAccessException {

		J customConvertedBean = customConverter(entity);
		if (customConvertedBean == null) {
			J convertedBean = this.jsonClazz.newInstance();
			BeanUtils.copyProperties(entity, convertedBean);
			return convertedBean;

		}

		return customConvertedBean;

	}

	protected List<J> converter(List<E> entities)
			throws InstantiationException, IllegalAccessException {

		List<J> jsonList = new ArrayList<J>();

		for (E entity : entities) {
			jsonList.add(converter(entity));
		}

		return jsonList;

	}

	@GET
	public Response findAll() throws InstantiationException,
			IllegalAccessException {
		return Response.status(Status.OK)
				.entity(converter(service().findAll())).build();
	}

	/**
	 * @param id
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@GET
	@Path("/{id}")
	public Response findById(@PathParam("id") String id)
			throws InstantiationException, IllegalAccessException {
		return Response.status(Status.OK)
				.entity(converter(service().findById(id))).build();
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") String id) {
		service().deleteById(id);
		return Response.status(Status.OK).build();
	}

	@POST
	public Response save(J json) throws InstantiationException,
			IllegalAccessException {
		return Response.status(Status.OK)
				.entity(converter(service().save(converter(json)))).build();
	}

	@PUT
	@Path("/{id}")
	public Response update(@PathParam("id") String id, J json)
			throws InstantiationException, IllegalAccessException {

		E entity = converter(json);
		entity.setId(id);

		return Response.status(Status.OK)
				.entity(converter(service().update(entity))).build();
	}
}
