package com.cjm.nt.facade.json;

public abstract class AbstractJson implements Json {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
