package com.cjm.nt.facade.json;

public class NoteJson extends AbstractJson {

	private String description;
	private String note;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
