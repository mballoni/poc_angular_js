package com.cjm.nt.facade;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjm.nt.entity.Note;
import com.cjm.nt.entity.User;
import com.cjm.nt.facade.json.NoteJson;
import com.cjm.nt.service.CrudService;
import com.cjm.nt.service.NoteService;
import com.cjm.nt.service.UserService;

@Component
@Path("/note")
public class NoteFacade extends CrudAbstractFacade<NoteJson, Note> {

	public NoteFacade() {
		super(NoteJson.class, Note.class);
	}

	@Autowired
	private NoteService noteService;

	@Autowired
	private UserService userService;

	@Override
	protected CrudService<Note> service() {
		return this.noteService;
	}

	@Override
	protected Note customConverter(NoteJson json) {
		return null;
	}

	@Override
	protected NoteJson customConverter(Note entity) {
		return null;
	}

	@GET
	public Response findAll(@CookieParam(value = COOKIE_GUID) String guid)
			throws InstantiationException, IllegalAccessException {

		User user = this.userService.findById(guid);

		return Response.status(Status.OK)
				.entity(converter(this.noteService.findAll(user))).build();
	}

	@GET
	@Path("/{id}")
	public Response findById(@CookieParam(value = COOKIE_GUID) String guid,
			@PathParam("id") String id) throws InstantiationException,
			IllegalAccessException {

		User user = this.userService.findById(guid);

		return Response.status(Status.OK)
				.entity(converter(this.noteService.findById(user, id))).build();
	}

	@POST
	public Response save(@CookieParam(value = COOKIE_GUID) String guid,
			NoteJson json) throws InstantiationException,
			IllegalAccessException {

		Note note = converter(json);
		note.setOwner(this.userService.findById(guid));

		return Response.status(Status.OK)
				.entity(converter(this.noteService.save(note))).build();
	}

	@PUT
	@Path("/{id}")
	public Response update(@CookieParam(value = COOKIE_GUID) String guid,
			@PathParam("id") String id, NoteJson json)
			throws InstantiationException, IllegalAccessException {

		User user = this.userService.findById(guid);

		if (this.noteService.findById(user, id) == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Note entity = converter(json);
		entity.setId(id);

		return Response.status(Status.OK)
				.entity(converter(this.noteService.update(entity))).build();
	}

}
