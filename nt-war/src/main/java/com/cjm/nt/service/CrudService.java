package com.cjm.nt.service;

import java.util.List;

import com.cjm.nt.entity.Entity;

public interface CrudService<E extends Entity> {

	E save(E entity);

	E update(E entity);

	List<E> findAll();

	E findById(String id);

	void delete(E entity);

	void deleteById(String id);

}