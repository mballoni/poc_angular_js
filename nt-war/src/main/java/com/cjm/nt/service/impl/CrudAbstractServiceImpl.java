package com.cjm.nt.service.impl;

import java.util.List;

import com.cjm.nt.dao.Dao;
import com.cjm.nt.entity.Entity;
import com.cjm.nt.service.CrudService;

public abstract class CrudAbstractServiceImpl<E extends Entity> implements
		CrudService<E> {

	protected abstract Dao<E> dao();

	public E save(E entity) {
		return dao().save(entity);
	}

	public E update(E entity) {
		return dao().update(entity);
	}

	public List<E> findAll() {
		return dao().findAll();
	}

	public E findById(String id) {
		return dao().findById(id);
	}

	public void delete(E entity) {
		dao().delete(entity);
	}

	public void deleteById(String id) {
		dao().deleteById(id);
	}

}
