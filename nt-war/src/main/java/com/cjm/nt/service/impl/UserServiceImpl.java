package com.cjm.nt.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjm.nt.dao.Dao;
import com.cjm.nt.dao.UserDao;
import com.cjm.nt.entity.User;
import com.cjm.nt.service.UserService;

@Component
public class UserServiceImpl extends CrudAbstractServiceImpl<User> implements
		UserService {

	@Autowired
	private UserDao userDao;

	@Override
	protected Dao<User> dao() {
		return this.userDao;
	}

}
