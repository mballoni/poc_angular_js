package com.cjm.nt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjm.nt.dao.Dao;
import com.cjm.nt.dao.NoteDao;
import com.cjm.nt.entity.Note;
import com.cjm.nt.entity.User;
import com.cjm.nt.service.NoteService;

@Component
public class NoteServiceImpl extends CrudAbstractServiceImpl<Note> implements
		NoteService {

	@Autowired
	private NoteDao noteDao;

	@Override
	protected Dao<Note> dao() {
		return this.noteDao;
	}

	public List<Note> findAll(User user) {
		return this.noteDao.findAll(user);
	}

	public Note findById(User user, String id) {
		return this.noteDao.findById(user, id);
	}

}
