package com.cjm.nt.service;

import java.util.List;

import com.cjm.nt.entity.Note;
import com.cjm.nt.entity.User;

public interface NoteService extends CrudService<Note> {

	List<Note> findAll(User user);

	Note findById(User user, String id);

}
