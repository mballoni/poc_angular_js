appengine-skeleton
=============================

[primeiro deploy]
http://cmiranda-nt.appspot.com/

[eclipse - kepler]
https://www.eclipse.org/

[gae dashboard]
https://appengine.google.com/

[maven]
http://maven.apache.org/

[maven no gae]
https://developers.google.com/appengine/docs/java/tools/maven

[datastore persistencia]
https://code.google.com/p/twig-persist/

[preços e cotas de serviços]
https://developers.google.com/appengine/pricing
https://developers.google.com/appengine/docs/quotas

[IoC (DI) - Spring]
http://projects.spring.io/spring-framework/
spring-web
spring-context...

[REST]
http://www.jboss.org/resteasy

[apresentação = static html + js + REST]
http://angularjs.org/
http://jquery.com/
http://underscorejs.org
http://requirejs.org/ (avaliar)
http://gruntjs.com/ (avaliar)
http://yeoman.io/ (avaliar)
